<%@ page language="java" contentType="text/html;charset=utf-8"
         pageEncoding="ISO-8859-1" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <link href="<c:url value='../../resources/css/style.css' />" rel="stylesheet">
    <link href="<c:url value='../../resources/css/bootstrap.css' />" rel="stylesheet">

    <title>${userName}</title>

    <script language="javascript" type="text/javascript"
            src="http://ajax.googleapis.com/ajax/libs/jquery/1.2.6/jquery.min.js"></script>
    <script language="javascript" type="text/javascript"
            src="../../resources/js/Main.js"></script>
</head>

<body>

<div id="header">

    <div id="social">
        <a href="#"><img src="../../resources/styleimages/facebook.png" width="26" height="26" alt="facebook"/></a>
        <a href="#"><img src="../../resources/styleimages/twitter.png" width="26" height="26" alt="twitter"/></a>
        <a href="#"><img src="../../resources/styleimages/linkedin.png" width="26" height="26" alt="linkedin"/></a>
        <a href="#"><img src="../../resources/styleimages/email.png" width="26" height="26" alt="email"/></a>
    </div>

</div>


<ul class="dropdown"><!-- menu -->

    <li><a href="${pageContext.request.contextPath}/">Home</a></li>

    <li><a href="${pageContext.request.contextPath}/gallery">Gallery</a>
        <ul class="sub_menu">
            <li><a>Category</a>
                <ul>
                    <c:forEach items="${categoriesList}" var="category">
                        <li>
                            <a href="${pageContext.request.contextPath}/gallery?sort=category&value=${category}">${category}</a>
                        </li>
                    </c:forEach>
                </ul>
            </li>
            <li><a href="${pageContext.request.contextPath}/gallery?sort=state&value=Exhibition">Exhibition</a></li>
            <li><a href="${pageContext.request.contextPath}/gallery?sort=state&value=On%20Export">On export</a></li>
        </ul>
    </li>
    <!-- end submenu -->
    <li><a href="${pageContext.request.contextPath}/admin/saveUpdate?id=0">NewExhibit</a></li>
    <li><a href="${pageContext.request.contextPath}/admin/exhibitManagement">Admin Panel</a></li>
</ul><!-- close menu -->


<div id="gallery"><!-- gallery -->

    <ul>
        <c:forEach items="${exhibitList}" var="exhibit">
            <li class="fancybox">
                <a href="${pageContext.request.contextPath}/page?id=${exhibit.getIdExhibit()}">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <h3 class="panel-title">${exhibit.title}</h3>
                        </div>
                        <div class="panel-body">
                            <img class="small-image-gallery" src="${exhibit.image}" width="100px"
                                 height="100px" alt="loading.."/>
                        </div>
                    </div>
                </a>
            </li>
        </c:forEach>
    </ul>

</div><!-- close gallery -->


<div id="intro">
    <p><span>Welcome.</span></p>
</div>


<div id="box_left">
    <h3>About</h3>
    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent cursus dolor hendrerit lacus egestas
        sollicitudin. Nullam pharetra commodo sem, sit amet mattis metus vestibulum at. Suspendisse in tellus metus. Ut
        lobortis gravida magna, et mollis nisl luctus et. Ut quis neque eros</p>
    <a><img src="../../static/images/more.png" class="more" width="68" height="24" alt="more"/></a>
</div>

<div id="box_right">
    <h3>Recent</h3>
    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent cursus dolor hendrerit lacus egestas
        sollicitudin. Nullam pharetra commodo sem, sit amet mattis metus vestibulum at. Suspendisse in tellus metus. Ut
        lobortis gravida magna, et mollis nisl luctus et. Ut quis neque eros</p>
    <a><img src="../../static/images/more.png" class="more" width="68" height="24" alt="more"/></a>
</div>

<div id="box_middle">
    <h3>Services</h3>
    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent cursus dolor hendrerit lacus egestas
        sollicitudin. Nullam pharetra commodo sem, sit amet mattis metus vestibulum at. Suspendisse in tellus metus. Ut
        lobortis gravida magna, et mollis nisl luctus et. Ut quis neque eros</p>
    <a><img src="../../static/images/more.png" class="more" width="68" height="24" alt="more"/></a>
</div>

<div id="footer">
</div>

</body>
</html>
