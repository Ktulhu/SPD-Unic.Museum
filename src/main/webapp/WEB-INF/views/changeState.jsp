<%@ page language="java" contentType="text/html; charset=utf-8"
         pageEncoding="ISO-8859-1" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <link href="<c:url value='../../resources/css/style.css' />" rel="stylesheet">
    <link href="<c:url value='../../resources/css/bootstrap.css' />" rel="stylesheet">
    <link href="<c:url value='../../resources/css/changeState.css' />" rel="stylesheet">
    <title>Museum</title>

    <script language="javascript" type="text/javascript"
            src="http://ajax.googleapis.com/ajax/libs/jquery/1.2.6/jquery.min.js"></script>
    <script language="javascript" type="text/javascript"
            src="../../resources/js/changeState.js"></script>
</head>

<body>

<div id="header">

    <div id="social">
        <a href="#"><img src="../../resources/styleimages/facebook.png" width="26" height="26" alt="facebook"/></a>
        <a href="#"><img src="../../resources/styleimages/twitter.png" width="26" height="26" alt="twitter"/></a>
        <a href="#"><img src="../../resources/styleimages/linkedin.png" width="26" height="26" alt="linkedin"/></a>
        <a href="#"><img src="../../resources/styleimages/email.png" width="26" height="26" alt="email"/></a>
    </div>

</div>


<ul class="dropdown"><!-- menu -->

    <li><a href="${pageContext.request.contextPath}/">Home</a></li>

    <li><a href="${pageContext.request.contextPath}/gallery">Gallery</a>
        <ul class="sub_menu">
            <li><a>Category</a>
                <ul>
                    <c:forEach items="${categoriesList}" var="category">
                        <li>
                            <a href="${pageContext.request.contextPath}/gallery?sort=category&value=${category}">${category}</a>
                        </li>
                    </c:forEach>
                </ul>
            </li>
            <li><a href="${pageContext.request.contextPath}/gallery?sort=state&value=Exhibition">Exhibition</a></li>
            <li><a href="${pageContext.request.contextPath}/gallery?sort=state&value=On%20Export">On export</a></li>
        </ul>
    </li>
    <!-- end submenu -->
    <li><a href="${pageContext.request.contextPath}/admin/saveUpdate?id=0">NewExhibit</a></li>
    <li><a href="${pageContext.request.contextPath}/admin/exhibitManagement">Admin panel</a></li>
</ul><!-- close menu -->

<div id="galeryWraper">
    <div id="gallery"><!-- gallery -->
        <span class="label label-success">${Success}</span>
        <span>Curent location : ${exhibitInfo.exhibit.state}| ${exhibitInfo.locationDescription}(ID: ${exhibitInfo.locationValue})</span>
        <br/>
        <form action="${pageContext.request.contextPath}/admin/changeState" method="post">
            <input value="${exhibitInfo.exhibit.getIdExhibit()}" name="idExhibit" type="hidden"/>
            <input type="hidden" id="csrfToken" name="${_csrf.parameterName}"
                   value="${_csrf.token}"/>
            <div class="row">
                <div class="form-group col-md-12">
                    <label class="col-md-3 control-lable">State</label>
                    <div class="col-sm-12">
                        <div class="radio">
                            <label for="radioStateOne">exhibition:</label>
                            <input type="radio" id="radioStateOne" name="newState" value="exhibition" onclick="ShowValuesList('museum')" required><br/>
                        </div>
                        <div class="radio">
                            <label for="radioStateTwo">onStock:</label>
                            <input type="radio" id="radioStateTwo" name="newState" value="onStock" onclick="ShowValuesList('stock')"><br/>
                        </div>
                        <div class="radio">
                            <label for="radioStateThree">onExport:</label>
                            <input type="radio" id="radioStateThree" name="newState" value="onExport" onclick="ShowValuesList('export')"><br/>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="form-group col-md-12">
                    <label class="col-md-3 control-lable">Value</label>
                    <div class="col-sm-12">

                        <div id="statePagePlaceListContainer">

                                <c:forEach items="${placeList}" var="place">
                                    <div class="radio">
                                    <label for="${place.id_place}">${place.place_name}</label>
                                    <input type="radio" id="${place.id_place}" value="${place.id_place}"
                                           name="idPlace">
                                    </div>
                                </c:forEach>

                        </div>
                        <br/>
                        <div id="statePageOwnersListContainer">
                                <c:forEach items="${ownersList}" var="owner">
                                    <div class="radio">
                                    <label for="${owner.id_owner}">${owner.fio}</label>
                                    <input type="radio" id="${owner.id_owner}" value="${owner.id_owner}"
                                           name="idOwner">
                                    </div>
                                </c:forEach>
                        </div>
                        <br/>
                        <div id="statePageStoresListContainer">
                                <c:forEach items="${storeSectionsList}" var="storeSection">
                                    <div class="radio">
                                    <label for="${storeSection.id_section}">${storeSection.section}:</label>
                                    <input type="radio" id="${storeSection.id_section}"
                                           value="${storeSection.id_section}" name="idSection">
                                    </div>
                                </c:forEach>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="form-group col-md-12">
                    <div class="col-sm-12">
                        <input type="submit" value="Update State" class="btn btn-primary"/>
                    </div>
                </div>
            </div>
        </form>
    </div>
    </div>
<!-- close gallery -->
<div id="footer">

</div>

</body>
</html>