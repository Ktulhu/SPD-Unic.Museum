<%@ page language="java" contentType="text/html; charset=utf-8"
         pageEncoding="ISO-8859-1" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <link href="<c:url value='../../resources/css/style.css' />" rel="stylesheet">
    <link href="<c:url value='../../resources/css/modalForm.css' />" rel="stylesheet">
    <link href="<c:url value='../../resources/css/bootstrap.css' />" rel="stylesheet">
    <link href="<c:url value='../../resources/css/jquery.fancybox-1.3.0.css' />" rel="stylesheet">
    <title>${userName}</title>

    <script language="javascript" type="text/javascript"
            src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script type="text/javascript" src="../../resources/js/jquery.fancybox-1.3.0.pack.js"></script>
    <script type="text/javascript" src="../../resources/js/jquery.redirect.js"></script>
    <script type="text/javascript" src="../../resources/js/exhibitManagement.js"></script>
</head>

<body>
<div id="header">

    <div id="social">
        <a href="#"><img src="../../resources/styleimages/facebook.png" width="26" height="26" alt="facebook"/></a>
        <a href="#"><img src="../../resources/styleimages/twitter.png" width="26" height="26" alt="twitter"/></a>
        <a href="#"><img src="../../resources/styleimages/linkedin.png" width="26" height="26" alt="linkedin"/></a>
        <a href="#"><img src="../../resources/styleimages/email.png" width="26" height="26" alt="email"/></a>
    </div>

</div>


<ul class="dropdown"><!-- menu -->

    <li><a href="${pageContext.request.contextPath}/">Home</a></li>

    <li><a href="${pageContext.request.contextPath}/gallery">Gallery</a>
        <ul class="sub_menu">
            <li><a>Category</a>
                <ul>
                    <c:forEach items="${categoriesList}" var="category">
                        <li>
                            <a href="${pageContext.request.contextPath}/gallery?sort=category&value=${category}">${category}</a>
                        </li>
                    </c:forEach>
                </ul>
            </li>
            <li><a href="${pageContext.request.contextPath}/gallery?sort=state&value=Exhibition">Exhibition</a></li>
            <li><a href="${pageContext.request.contextPath}/gallery?sort=state&value=On%20Export">On export</a></li>
        </ul>
    </li>
    <!-- end submenu -->
    <li><a href="${pageContext.request.contextPath}/admin/saveUpdate?id=0">NewExhibit</a></li>
    <li><a href="${pageContext.request.contextPath}/admin/exhibitManagement">Admin panel</a></li>
</ul><!-- close menu -->

<div id="wraper">
    <div id="content"><!-- content -->
        <input type="hidden" id="csrfToken" name="${_csrf.parameterName}"
               value="${_csrf.token}" />
        <ul>
            <c:forEach items="${exhibitList}" var="exhibit">
                <li>
                    <div class="managementedItem" id="exhibitNumber${exhibit.getIdExhibit()}">
                        <div class="leftManagmentContentWraper">
                            <img src="${exhibit.image}" alt="Loading.."/>
                            <div class="leftInnerManagmentContentWraper">
                                <div class="titleExhibit">${exhibit.title}</div>
                                <div class="shortAboutExhibit">${exhibit.about}
                                </div>
                            </div>
                        </div>
                        <hr noshade>
                        <div class="rightManagmentContentWraper">
                            <div class="categoryExhibit">${exhibit.category}</div>
                            <div class="stateExhibit">${exhibit.state}</div>
                            <div class="modifyButton btn btn-info" onclick="ModifyAlert(${exhibit.getIdExhibit()},this)">
                                Modify
                            </div>
                        </div>
                    </div>
                </li>
            </c:forEach>
        </ul>
    </div><!-- end content -->


    <div id="sidebar"><!-- sidebar -->

        <h3>Recent</h3>

        <div class="navcontainer">
            <ul>
                <li><a onclick="getStoredExhibits()">Show only stored exhibits</a></li>
                <li><a onclick="getMuseumExhibits()">Show only exhibits on exhibition</a></li>
                <li><a onclick="getExportExhibits()">Show only export exhibits</a></li>
            </ul>
        </div>
    </div><!-- end sidebar -->
</div>

<div id="footer">
    Exclusively on <a href="http://www.sixrevisions.com">Six Revisions</a> | Created by <a
        href="http://www.csstemplateheaven.com">CssTemplateHeaven</a>
</div>
<%--modal form--%>
<div id="modal_form">
    <span id="modal_close">X</span>
    <div id="modalFormExhibitID"></div>
        <div id="modalFormWraper">
            <div id="modalFormLeftContainer">
                <img id="modalFormImage" src="" alt="Loading.."/>
                <div id="modalFormState" class = "panel panel-warning">
                    <div id="modalFormStateBody" class = "panel-heading">
                    </div>
                </div>
                <div id="modalFormCategory" class = "panel panel-warning">
                    <div id="modalFormCategoryBody" class = "panel-heading">
                    </div>
                </div>
            </div>
                    <div id="modalFormInformationContainer">
                <div id="modalFormTitle" class="titleExhibit"></div>
                <div id="modalFormAbout" class="shortAboutExhibit"></div>
                    </div>
                 <div id="modalFormButtonsContainer">
                     <div id="modalFormButtonsContainerForButtons">
            <div id="modalFormChangeStateButton" class="btn btn-info" onclick="ChangeState()">ChangeState</div>
            <a id="modalFormChangeGeneralInformationButton" href="" class="btn btn-info">Change general information</a>
                         <div id="modalFormChangePLaceButton" class="btn btn-info" onclick="ShowPlaces()">ChangPlace</div>
                     </div>
                     <div id="modalFormButtonsContainerForLists">
                     <div id="modalFormListPLacesContainer" class="list-group">
                         <ul>
                             <c:forEach items="${placeList}" var="place">
                             <li class="list-group-item" onclick="ChangePlace(${place.id_place})">${place.place_name}</li>
                             </c:forEach>
                         </ul>
                     </div>
                     </div>
                 </div>
        </div>
        </div>

    <div id="overlay"></div>

</div>
</body>
</html>
