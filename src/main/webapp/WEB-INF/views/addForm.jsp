<%@ page language="java" contentType="text/html; charset=utf-8"
         pageEncoding="ISO-8859-1" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <link href="<c:url value='../../resources/css/style.css' />" rel="stylesheet">
    <link href="<c:url value='../../resources/css/bootstrap.css' />" rel="stylesheet">
    <title>Museum</title>

    <script language="javascript" type="text/javascript"
            src="http://ajax.googleapis.com/ajax/libs/jquery/1.2.6/jquery.min.js"></script>
    <script language="javascript" type="text/javascript"
            src="../../resources/js/Main.js"></script>
</head>

<body>

<div id="header">

    <div id="social">
        <a href="#"><img src="../../resources/styleimages/facebook.png" width="26" height="26" alt="facebook"/></a>
        <a href="#"><img src="../../resources/styleimages/twitter.png" width="26" height="26" alt="twitter"/></a>
        <a href="#"><img src="../../resources/styleimages/linkedin.png" width="26" height="26" alt="linkedin"/></a>
        <a href="#"><img src="../../resources/styleimages/email.png" width="26" height="26" alt="email"/></a>
    </div>

</div>


<ul class="dropdown"><!-- menu -->

    <li><a href="${pageContext.request.contextPath}/">Home</a></li>

    <li><a href="${pageContext.request.contextPath}/gallery">Gallery</a>
        <ul class="sub_menu">
            <li><a>Category</a>
                <ul>
                    <c:forEach items="${categoriesList}" var="category">
                        <li>
                            <a href="${pageContext.request.contextPath}/gallery?sort=category&value=${category}">${category}</a>
                        </li>
                    </c:forEach>
                </ul>
            </li>
            <li><a href="${pageContext.request.contextPath}/gallery?sort=state&value=Exhibition">Exhibition</a></li>
            <li><a href="${pageContext.request.contextPath}/gallery?sort=state&value=On%20Export">On export</a></li>
        </ul>
    </li>
    <!-- end submenu -->
    <li><a href="${pageContext.request.contextPath}/admin/saveUpdate?id=0">NewExhibit</a></li>
    <li><a href="${pageContext.request.contextPath}/admin/exhibitManagement">Admin panel</a></li>
</ul><!-- close menu -->

<div id="galeryWraper">
    <div id="gallery"><!-- gallery -->
        <span class="label label-success">${Success}</span>
        <br/>
        <form:form action="saveUpdate" method="post" class="form-horizontal" modelAttribute="exhibit">
        <form:input id="id_exhibit" path="id_exhibit" type="hidden" value="${exhibit.id_exhibit+0}"/>
        <input type="hidden" id="csrfToken" name="${_csrf.parameterName}"
               value="${_csrf.token}" />
        <div class="row">
            <div class="form-group col-md-12">
                <label class="col-md-3 control-lable" for="Title">Title</label>

                <div class="col-sm-12">
                    <form:input type="text" class="form-control" path="title" name="title" id="Title"
                                value="${exhibit.title}"/>
                    <div class="has-error">
                        <form:errors path="title" class="help-inline"/>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="form-group col-md-12">
                <label class="col-md-3 control-lable" for="about">About</label>
                <div class="col-sm-12">
                    <form:input type="text" class="form-control" id="about" path="about" value="${exhibit.title}"/>


                    <div class="has-error">
                        <form:errors path="about" class="help-inline"/>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="form-group col-md-12">
                <label class="col-md-3 control-lable">Category</label>
                <div class="col-sm-12">

                    <c:forEach items="${categoriesList}" var="cat">
                        <div class="radio">
                            <label>
                                <form:radiobutton value="${cat}" path="category"/>${cat}
                            </label>
                        </div>
                    </c:forEach>
                    <div class="has-error">
                        <form:errors path="category" class="help-inline"/>
                    </div>
                </div>
            </div>
        </div>
        <div id="addFormImageSubmitOrganization">
            <div class="form-group">
                <div class="col-sm-offset-0 col-sm-10 submitMargin">
                    <input type="submit" value="Save Exhibit" class="btn btn-primary"/>
                </div>
            </div>

            <form:input id="image_from_list" name="image" type="hidden" value="" path="image"/>

            </form:form>
            <form:form id="forma" modelAttribute="image" target="frame_ajax" action="saveImage?_csrf=${_csrf.token}"
                       enctype="multipart/form-data">
                <div id="uploadPictureOrganization">
                    <input id="UploadFile" name="files[0]" type="file"/>
                    <button class="btn btn-primary" name="Up" value="Upload" onclick="Upload()">Upload</button>
                </div>
            </form:form>
            <iframe name="frame_ajax" src="http://localhost:8080/saveImage" width="0" height="0" style="display:none">
            </iframe>

            <img id="image_container" alt="none load" src=""/>
            <div class="has-error">
                <form:errors path="image" class="help-inline"/>
            </div>

        </div>
    </div>
</div>
<!-- close gallery -->
<div id="footer">

</div>

</body>
</html>
