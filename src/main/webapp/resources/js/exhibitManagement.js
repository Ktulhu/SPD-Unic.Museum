var token;
$(window).scroll(function() {
    if ($(this).scrollTop()>240) $('#sidebar').css({'position':'fixed',
        'top':'0px'});
    else $('#sidebar').css({'top':'240px', 'position':'absolute'});
});

$(document).ready(function () {
    token = $("#csrfToken").val();
    $('#modal_close, #overlay').click( function(){
        $('#modal_form')
            .animate({opacity: 0, top: '45%'}, 200,
                function(){
                    $(this).css('display', 'none');
                    $('#overlay').fadeOut(400);
                }
            );
    });
});

function getStoredExhibits() {
    $.post("/admin/getStoredExhibits?_csrf="+token,
        {},
        function (exhibitList) {

        });
}

function getMuseumExhibits() {
    $.post("/admin/getMuseumExhibits?_csrf="+token,
        {},
        function (exhibitList) {

        });
}

function getExportExhibits() {
    $.post("/admin/getExportExhibits?_csrf="+token,
        {},
        function (exhibitList) {

        });
}

function ModifyAlert(idExhibit,e) {
    var el;
    el = $("#exhibitNumber" + idExhibit);
    var left = el.offset().left;
    var top = el.offset().top;
    // var left = e.offsetLeft;
    // var left2 = $("#exhibitNumber"+13).id;
    console.log(left);
    $.post("/admin/getExInfo?_csrf=" + token,
        {
            id: idExhibit
        },
        function (response) {
            var exhibit = jQuery.parseJSON(response);
            // event.preventDefault();
            $('#overlay').fadeIn(400,
                function () {
                    $('#modal_form')
                        .css({'display':'block'})
                        .animate({opacity: 1, top: '65%'}, 200);

                });
            $("#modalFormTitle").text(exhibit.title);
            $("#modalFormAbout").text(exhibit.about);
            $("#modalFormStateBody").text(exhibit.state);
            $("#modalFormCategoryBody").text(exhibit.category);
            $("#modalFormImage").attr("src",exhibit.image);
            $("#modalFormExhibitID").text(exhibit.id_exhibit);
            $("#modalFormHrefImg").attr("href","/page?id="+exhibit.id_exhibit);
            if(exhibit.state!=="exhibition"){
                $("#modalFormChangePLaceButton").css({'display': 'none'});
            }
        });
}

function ShowPlaces() {
    if($('#modalFormListPLacesContainer').css('display')==="none"){
        $('#modalFormListPLacesContainer')
            .css({'display':'block'})
            .animate({opacity: 1}, 200);
    }else{
        $('#modalFormListPLacesContainer')
            .animate({opacity: 0}, 200)
            .css({'display':'none'});
    }
}

function ChangePlace(id_place) {
    var idExhibit = $("modalFormExhibitID").text();
    $.post("/admin/changePlace?_csrf="+token,
        {
            idExhibit: idExhibit,
            idPlace: id_place
        });
}

function ChangeState() {
    var idExhibit = $("#modalFormExhibitID").text();
    $.redirect("/admin/modifyStatePage?_csrf="+token,
        {
            idExhibit: idExhibit
        },"POST");
}
