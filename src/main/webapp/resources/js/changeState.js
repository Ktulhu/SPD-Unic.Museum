var colapsed = true;
function ShowValuesList(state){
    switch (state){
        case "museum":{
            if(colapsed){
                $('#statePagePlaceListContainer')
                    .css({'display':'block'})
                    .animate({opacity: 1}, 200);
                colapsed = false;
            }else {
                if ($('#statePagePlaceListContainer').css('display')==="block"){
                }else {
                    collapseAllLists();
                    $('#statePagePlaceListContainer')
                        .css({'display':'block'})
                        .animate({opacity: 1}, 200);
                    colapsed = false
                }
            }
            break;
        }
        case "stock":{
            if(colapsed){
                $('#statePageStoresListContainer')
                    .css({'display':'block'})
                    .animate({opacity: 1}, 200);
                colapsed = false;
            }else {
                if ($('#statePageStoresListContainer').css('display')==="block"){
                }else {
                    collapseAllLists();
                    $('#statePageStoresListContainer')
                        .css({'display':'block'})
                        .animate({opacity: 1}, 200);
                    colapsed = false;
                }
            }
            break;
        }
        case "export":{
            if(colapsed){
                $('#statePageOwnersListContainer')
                    .css({'display':'block'})
                    .animate({opacity: 1}, 200);
                colapsed = false;
            }else {
                if ($('#statePageOwnersListContainer').css('display')==="block"){
                }else {
                    collapseAllLists();
                    $('#statePageOwnersListContainer')
                        .css({'display':'block'})
                        .animate({opacity: 1}, 200);
                    colapsed = false;
                }
            }
            break;
        }
    }
}
function collapseAllLists() {
    $('#statePagePlaceListContainer')
        .animate({opacity: 0}, 200)
        .css({'display':'none'});
    $('#statePageOwnersListContainer')
        .animate({opacity: 0}, 200)
        .css({'display':'none'});
    $('#statePageStoresListContainer')
        .animate({opacity: 0}, 200)
        .css({'display':'none'});
    colapsed = true;
}


