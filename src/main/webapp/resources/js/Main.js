function Upload() {
    var token = $("#csrfToken").val();
    var $form = $(this);
    var File1 = document.getElementById("UploadFile");

    var subStringFileName = File1.value.toString().substring(File1.value.toString().search('fakepath'), File1.value.toString().length);
    var subStringFileName2 = subStringFileName.substring(9, subStringFileName.length);
    $.post("/admin/saveImage?_csrf=" + token,
        $form.serialize(),
        function (data) {

            var img = $("#image_container");
            $(img).attr("src", "\\resources\\images\\" + subStringFileName2 + "");
            $('#image_from_list').val($(img).attr("src"));
            $('#data_value').val($(img).attr("id"));
        }
    );
}
