package com.museum.controller;

import com.museum.model.Enums.State;
import com.museum.model.Exhibit;
import com.museum.service.ExhibitService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

@Controller
public class AppController {

    private final
    ExhibitService exhibitService;

    @Autowired
    public AppController(ExhibitService exhibitService) {
        this.exhibitService = exhibitService;
    }

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String indexPage(ModelMap model) {
        model.addAttribute("userName", getUserNameFromPrincipal());
        List exhibits = exhibitService.getFixedAmountExhibit(4);
        List categoryList = exhibitService.getAllCategories();
        model.addAttribute("categoriesList", categoryList);
        model.addAttribute("exhibitList", exhibits);
        return "index";
    }

    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public String login() {
        return "login";
    }

    @RequestMapping(value = "/logout", method = RequestMethod.GET)
    public String logoutPage(HttpServletRequest request, HttpServletResponse response) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (auth != null) {
            new SecurityContextLogoutHandler().logout(request, response, auth);
        }
        return "redirect:/login?logout";
    }

    @RequestMapping(value = "/gallery", method = RequestMethod.GET)
    public String gallery(ModelMap model,
                          @RequestParam(value = "sort", required = false) String sort,
                          @RequestParam(value = "value", required = false) String value) {
        if (sort == null || value == null) {
            sort = "default";
            value = "default";
        }
        model.addAttribute("userName", getUserNameFromPrincipal());
        List categoryList = exhibitService.getAllCategories();
        List<Exhibit> exhibits;
        switch (sort) {
            case "category": {
                exhibits = exhibitService.getListExhibitsByCategoryExceptStored(value);
                break;
            }
            case "state": {
                if (value.equals("Exhibition")) {
                    exhibits = exhibitService.getExhibitsByState(State.exhibition);
                } else {
                    exhibits = exhibitService.getExhibitsByState(State.onExport);
                }
                break;
            }
            default: {
                exhibits = exhibitService.getExhibitsByState(State.exhibition);
                exhibits.addAll(exhibitService.getExhibitsByState(State.onExport));
                break;
            }
        }
        model.addAttribute("categoriesList", categoryList);
        model.addAttribute("exhibitList", exhibits);
        return "gallery";
    }

    @RequestMapping(value = "/page", method = RequestMethod.GET)
    public String getExhibit(ModelMap model, int id) {
        Exhibit exhibit = exhibitService.getExhibitById(id);
        List categoryList = exhibitService.getAllCategories();
        model.addAttribute("categoriesList", categoryList);
        model.addAttribute("exhibit", exhibit);
        return "page";
    }

    @RequestMapping(value = "/accessDenied", method = RequestMethod.GET)
    public String accessDenied(ModelMap modelMap) {
        modelMap.addAttribute("username", getUserNameFromPrincipal());
        return "403";
    }

    private String getUserNameFromPrincipal() {
        String userName;
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if (principal instanceof UserDetails) {
            userName = ((UserDetails) principal).getUsername();
        } else {
            userName = principal.toString();
        }
        return userName;
    }
}
