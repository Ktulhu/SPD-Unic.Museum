package com.museum.controller;

import com.google.gson.Gson;
import com.museum.customExceptions.NotFoundException;
import com.museum.model.Categories;
import com.museum.model.CrunchifyFileUpload;
import com.museum.model.Enums.State;
import com.museum.model.Exhibit;
import com.museum.model.Place;
import com.museum.service.ExhibitService;
import com.museum.service.ImageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.io.IOException;
import java.util.List;
import java.util.concurrent.TimeUnit;

@Controller
public class AdminController {
    private final
    ExhibitService exhibitService;
    private final
    Gson gson;
    private final
    ImageService imageService;

    @Autowired
    public AdminController(ExhibitService exhibitService, Gson gson, ImageService imageService)
    {
        this.imageService = imageService;
        this.gson = gson;
        this.exhibitService = exhibitService;
    }


    @RequestMapping(value = "/admin/deleteExhibit", method = RequestMethod.GET)
    public String deleteExhibit(ModelMap model, int id) {
        exhibitService.deleteExhibitById(id);
        List exhibits = exhibitService.getAllExhibits();
        List categoryList = exhibitService.getAllCategories();
        model.addAttribute("exhibitList", exhibits);
        model.addAttribute("categoriesList", categoryList);
        return "gallery";
    }

    @RequestMapping(value = "/admin/saveUpdate", method = RequestMethod.POST)
    public String saveExhibit(@Valid Exhibit exhibit, BindingResult result,
                              ModelMap model, HttpServletRequest request) throws IOException {
        List categoryList = exhibitService.getAllCategories();
        model.addAttribute("categoriesList", categoryList);
        if (result.hasErrors()) {
            return "addForm";
        }
        if (exhibit.getIdExhibit() == 0) {
            exhibitService.saveExhibit(exhibit);
            model.addAttribute("Success", "Save success!");
        } else {
            exhibitService.updateExhibit(exhibit);
            model.addAttribute("Success", "Update success!");
        }
        imageService.deleteUnresolvedImages(request);
        return "addForm";
    }

    @RequestMapping(value = "/admin/saveUpdate", method = RequestMethod.GET)
    public String saveUpdate(ModelMap model, int id) {
        Exhibit exhibit;
        List categoryList = exhibitService.getAllCategories();
        if (id != 0) {
            exhibit = exhibitService.getExhibitById(id);
        } else {
            exhibit = new Exhibit();
        }
        model.addAttribute("exhibit", exhibit);
        model.addAttribute("categoriesList", categoryList);
        return "addForm";
    }

    @RequestMapping(value = "/admin/saveImage", method = RequestMethod.POST)
    @ResponseBody
    public String saveImage(@ModelAttribute("image") CrunchifyFileUpload image,
                            HttpServletRequest request) {
        try {
            TimeUnit.NANOSECONDS.sleep(10); //Сервер не встигає зберегти картинку, щоб потім вона відобразилась відразу AJAX-ом
            imageService.saveImages(request, image);//на сторінці
        } catch (InterruptedException | IOException e) {
            e.printStackTrace();
        }
        String[] feedBackImage = imageService.getImages(request);
        return feedBackImage[feedBackImage.length - 1];
    }

    @RequestMapping(value = "/admin/exhibitManagement", method = RequestMethod.GET)
    public String exhibitManagement(ModelMap modelMap) {
        List<Exhibit> exhibits = exhibitService.getAllExhibits();
        List<Categories> categories = exhibitService.getAllCategories();
        List<Place> placeList = exhibitService.getAllPlaces();
        State[] stateList = State.values();
        modelMap.addAttribute("exhibitList", exhibits);
        modelMap.addAttribute("categoriesList", categories);
        modelMap.addAttribute("userName", getUserNameFromPrincipal());
        modelMap.addAttribute("placeList", placeList);
        modelMap.addAttribute("stateList", stateList);
        return "exhibitManagement";
    }

    @ResponseBody
    @RequestMapping(value = "/admin/getExInfo", method = RequestMethod.POST)
    public String getExhibitInformation(Integer id) {
        Exhibit exhibit = exhibitService.getExhibitById(id);
        return gson.toJson(exhibit);
    }

    @ResponseBody
    @RequestMapping(value = "/admin/getStoredExhibits")
    public String getStoredExhibits() {
        List<Exhibit> exhibits = exhibitService.getExhibitsByState(State.onStock);
        return gson.toJson(exhibits);
    }

    @ResponseBody
    @RequestMapping(value = "/admin/getMuseumExhibits")
    public String getMuseumExhibits() {
        List<Exhibit> exhibits = exhibitService.getExhibitsByState(State.exhibition);
        return gson.toJson(exhibits);
    }

    @ResponseBody
    @RequestMapping(value = "/admin/getExportExhibits")
    public String getExportExhibits() {
        List<Exhibit> exhibits = exhibitService.getExhibitsByState(State.onExport);
        return gson.toJson(exhibits);
    }

    @ResponseBody
    @RequestMapping(value = "/admin/changePlace", method = RequestMethod.POST)
    public void changePlace(Integer idExhibit, Integer idPlace) {
        exhibitService.changePlace(idExhibit, idPlace);
    }

    @RequestMapping(value = "admin/modifyStatePage", method = RequestMethod.POST)
    public String modifyStatePage(Integer idExhibit, ModelMap modelMap) {
        try {
            modelMap.addAttribute("exhibitInfo", exhibitService.getFullInformationExhibit(idExhibit));
        } catch (NullPointerException e) {
            System.out.println("No such Exhibit found");
            throw new NotFoundException("No such exhibit available");
        }
        modelMap.addAttribute("placeList", exhibitService.getAllPlaces());
        modelMap.addAttribute("storeSectionsList", exhibitService.getAllStores());
        modelMap.addAttribute("ownersList", exhibitService.getAllOwners());
        return "changeState";
    }

    @RequestMapping(value = "/admin/changeState", method = RequestMethod.POST)
    public String changeState(Integer idExhibit, String newState, Integer idSection,
                              Integer idOwner, Integer idPlace) {
        State state = State.valueOf(newState);
        exhibitService.changeState(idExhibit, state, idSection, idOwner, idPlace);
        return "exhibitManagement";
    }

    private String getUserNameFromPrincipal() {
        String userName;
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if (principal instanceof UserDetails) {
            userName = ((UserDetails) principal).getUsername();
        } else {
            userName = principal.toString();
        }
        return userName;
    }
}

