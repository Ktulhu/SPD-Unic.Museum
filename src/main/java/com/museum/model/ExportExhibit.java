package com.museum.model;

import javax.persistence.*;

@Entity
@Table(name = "export_exhibit")
public class ExportExhibit implements Exhibitble {
    @Id
    private Integer id_exhibit;
    private Integer id_owner;

    public ExportExhibit() {
    }

    public Integer getId_owner() {
        return id_owner;
    }

    public void setId_owner(Integer place) {

        this.id_owner = place;
    }

    public ExportExhibit(Integer id_exhibit, Integer id_owner) {
        this.id_exhibit = id_exhibit;
        this.id_owner = id_owner;
    }

    @Override
    public Integer getIdExhibit() {
        return id_exhibit;
    }

    @Override
    public Integer getLocationValue() {
        return id_owner;
    }
}
