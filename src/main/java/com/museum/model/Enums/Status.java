package com.museum.model.Enums;

public enum Status {
    ACTIVE("Active"),
    INACTIVE("Inactive"),
    DELETED("Deleted"),
    LOCKED("Locked");

    private String status;

    private Status(final String status) {
        this.status = status;
    }
    @Override
    public String toString() {
        return "Status{" +
                "status='" + status + '\'' +
                '}';
    }

    public String getName() {
        return this.name();
    }

    public String getStatus() {
        return status;
    }
}
