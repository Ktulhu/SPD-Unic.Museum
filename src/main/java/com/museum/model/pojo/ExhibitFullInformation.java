package com.museum.model.pojo;

import com.museum.model.Exhibit;

import java.io.Serializable;

public class ExhibitFullInformation implements Serializable {

    private Exhibit exhibit;
    private Integer locationValue;
    private String locationDescription;

    public ExhibitFullInformation(Exhibit exhibit, Integer locationValue, String locationDescription) {
        this.exhibit = exhibit;
        this.locationValue = locationValue;
        this.locationDescription = locationDescription;
    }

    public Exhibit getExhibit() {
        return exhibit;
    }

    public void setExhibit(Exhibit exhibit) {
        this.exhibit = exhibit;
    }

    public Integer getLocationValue() {
        return locationValue;
    }

    public void setLocationValue(Integer locationValue) {
        this.locationValue = locationValue;
    }

    public String getLocationDescription() {
        return locationDescription;
    }

    public void setLocationDescription(String locationDescription) {
        this.locationDescription = locationDescription;
    }
}
