package com.museum.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "store")
public class Store {

    private Integer id_staff;

    @Id
    @GeneratedValue
    private Integer id_section;

    private String section;

    public Store() {
    }

    public Integer getId_staff() {
        return id_staff;
    }

    public void setId_staff(Integer id_staff) {
        this.id_staff = id_staff;
    }

    public Integer getId_section() {
        return id_section;
    }

    public void setId_section(Integer id_section) {
        this.id_section = id_section;
    }

    public String getSection() {
        return section;
    }

    public void setSection(String section) {
        this.section = section;
    }
}
