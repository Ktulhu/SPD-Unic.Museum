package com.museum.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "museum_exhibit")
public class MuseumExhibit implements Exhibitble {
    @Id
    private Integer id_exhibit;

    private Integer id_place;

    public MuseumExhibit() {
    }

    @Override
    public Integer getIdExhibit() {
        return id_exhibit;
    }

    public void setId_exhibit(Integer id_exhibit) {
        this.id_exhibit = id_exhibit;
    }

    @Override
    public Integer getLocationValue() {
        return id_place;
    }

    public void setId_place(Integer id_place) {
        this.id_place = id_place;
    }

    public MuseumExhibit(Integer id_exhibit, Integer id_place) {
        this.id_exhibit = id_exhibit;
        this.id_place = id_place;
    }

}
