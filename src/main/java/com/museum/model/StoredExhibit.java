package com.museum.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "stored_exhibit")
public class StoredExhibit implements Exhibitble {
    @Id
    private Integer id_exhibit;

    private Integer id_section;

    public StoredExhibit() {
    }

    public StoredExhibit(Integer id_exhibit, Integer id_section) {
        this.id_exhibit = id_exhibit;
        this.id_section = id_section;
    }

    public void setId_exhibit(Integer id_exhibit) {
        this.id_exhibit = id_exhibit;
    }

    public void setId_section(Integer id_section) {
        this.id_section = id_section;
    }

    @Override
    public Integer getIdExhibit() {
        return id_exhibit;
    }

    @Override
    public Integer getLocationValue() {
        return id_section;
    }
}
