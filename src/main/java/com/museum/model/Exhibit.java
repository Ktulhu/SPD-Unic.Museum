package com.museum.model;

import com.museum.model.Enums.State;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.*;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

@Entity
@XmlRootElement(name = "exhibit")
@Table(name = "exhibit")
public class Exhibit implements Serializable,Exhibitble {
    @Id
    @GeneratedValue
    private Integer id_exhibit;

    @Size(min = 3, max = 20)
    private String title;

    @Size(min = 3, max = 255)
    private String about;

    @NotEmpty
    private String category;

    @NotEmpty
    private String image;

    @Enumerated(EnumType.STRING)
    private State state = State.onStock;

    @OneToOne
    @PrimaryKeyJoinColumn
    private ExportExhibit exportExhibit;

    @OneToOne
    @PrimaryKeyJoinColumn
    private StoredExhibit storeExhibit;

    @OneToOne
    @PrimaryKeyJoinColumn
    private MuseumExhibit museumExhibit;

    public Exhibit() {
    }

    public StoredExhibit getStoreExhibit() {
        return storeExhibit;
    }

    public void setStoreExhibit(StoredExhibit storeExhibit) {
        this.storeExhibit = storeExhibit;
    }

    public MuseumExhibit getMuseumExhibit() {
        return museumExhibit;
    }

    public void setMuseumExhibit(MuseumExhibit museumExhibit) {
        this.museumExhibit = museumExhibit;
    }

    public ExportExhibit getExportExhibit() {
        return exportExhibit;
    }

    public void setExportExhibit(ExportExhibit exportExhibit) {
        this.exportExhibit = exportExhibit;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    @Override
    public Integer getIdExhibit() {
        return id_exhibit;
    }

    @Override
    public Integer getLocationValue() {
        return 0;
    }
    @XmlElement
    public void setId_exhibit(Integer id_exhibit) {
        this.id_exhibit = id_exhibit;
    }

    public String getTitle() {
        return title;
    }

    @XmlElement
    public void setTitle(String title) {
        this.title = title;
    }

    public String getAbout() {
        return about;
    }

    @XmlElement
    public void setAbout(String about) {
        this.about = about;
    }

    public String getCategory() {
        return category;
    }

    @XmlElement
    public void setCategory(String category) {
        this.category = category;
    }

    public State getState() {
        return state;
    }

    @XmlElement
    public void setState(State state) {
        this.state = state;
    }

    public Exhibit(String title, String about, String category, String image, State state) {
        this.title = title;
        this.about = about;
        this.category = category;
        this.image = image;
        this.state = state;
    }

    @Override
    public String toString() {
        return "id:" + id_exhibit +
                ", title:" + title +
                ", category:" + category;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        Exhibit exhibit = (Exhibit) o;

        return new org.apache.commons.lang.builder.EqualsBuilder()
                .append(id_exhibit, exhibit.id_exhibit)
                .append(title, exhibit.title)
                .append(about, exhibit.about)
                .append(category, exhibit.category)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(id_exhibit)
                .append(title)
                .append(about)
                .append(category)
                .toHashCode();
    }
}
