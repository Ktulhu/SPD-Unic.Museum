package com.museum.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "place")
public class Place implements Serializable {
    @Id
    @GeneratedValue
    private Integer id_place;
    private String place_name;
    private String about;
    private Integer id_staff;

    public Place() {
    }

    public Integer getId_place() {
        return id_place;
    }

    public void setId_place(Integer id_place) {
        this.id_place = id_place;
    }

    public String getPlace_name() {
        return place_name;
    }

    public void setPlace_name(String place_name) {
        this.place_name = place_name;
    }

    public String getAbout() {
        return about;
    }

    public void setAbout(String about) {
        this.about = about;
    }

    public Integer getId_staff() {
        return id_staff;
    }

    public void setId_staff(Integer id_staff) {
        this.id_staff = id_staff;
    }
}
