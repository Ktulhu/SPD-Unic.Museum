package com.museum.service;

import com.museum.dao.UserDao;
import com.museum.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Transactional
@Service("userService")
public class UserService {

    @Autowired
    UserDao userDao;

    public User findUserBySso(String sso_id) {
        return userDao.findUserBySso(sso_id);
    }

    public User findUserById(Integer id) {
        return userDao.findUserById(id);
    }
}
