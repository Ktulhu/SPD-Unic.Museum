package com.museum.service;

import com.museum.dao.ExhibitDao;
import com.museum.model.*;
import com.museum.model.Enums.State;
import com.museum.model.pojo.ExhibitFullInformation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@org.springframework.transaction.annotation.Transactional
public class ExhibitService {

    private final ExhibitDao exhibitDao;

    @Autowired
    public ExhibitService(@Qualifier(value = "exhibitDao") ExhibitDao exhibitDao) {
        this.exhibitDao = exhibitDao;
    }

    public Exhibit getExhibitById(int id) {
        return exhibitDao.getExhibitById(id);
    }

    public<T extends Exhibitble> void saveExhibit(T exhibit) {
        if (exhibit.getClass().equals(Exhibit.class)) {
            StoredExhibit storedExhibit = new StoredExhibit(exhibit.getIdExhibit(), 1);
            exhibitDao.save(exhibit);
            exhibitDao.save(storedExhibit);
        } else exhibitDao.save(exhibit);
    }

    public void updateExhibit(Exhibit exhibit) {
        Exhibit entity = exhibitDao.getExhibitById(exhibit.getIdExhibit());
        if (entity != null) {
            entity.setAbout(exhibit.getAbout());
            entity.setCategory(exhibit.getCategory());
            entity.setTitle(exhibit.getTitle());
            exhibitDao.update(entity);
        }
    }

    public void deleteExhibitById(int id) {
        Exhibit entity = exhibitDao.getExhibitById(id);
        if (entity != null) {
            exhibitDao.delete(entity);
        }
    }

    public List<Exhibit> getAllExhibits() {
        return exhibitDao.getAllExhibits();
    }

    public List getFixedAmountExhibit(int count) {
        return exhibitDao.getFixedAmountExhibit(count);
    }

    public List<Categories> getAllCategories() {
        return exhibitDao.getAllCategories();
    }

    public List getAllOwners() {
        return exhibitDao.getAllOwners();
    }

    public List<Store> getAllStores() {
        return exhibitDao.getAllStores();
    }

    public List<Place> getAllPlaces() {
        return exhibitDao.getAllPlaces();
    }

    public List<Exhibit> getListExhibitsByCategoryExceptStored(String categoty) {
        return exhibitDao.getListExhibitsByCategoryExceptStored(categoty);
    }

    public List getExhibitsByOwn(String fio) {
        return exhibitDao.getListExhibitsByOwn(fio);
    }

    public List<Exhibit> getExhibitsByState(State state) {
        switch (state) {
            case exhibition:{
                return exhibitDao.getMuseumExhibits();}
            case onStock: {
                return exhibitDao.getStoredExhibits();}
            case onExport: {
                return exhibitDao.getExportExhibits();}
            default:
                return null;
        }
    }

    public List<Exhibit> getExhibitsByCategoty(String categoty) {
        return exhibitDao.getListExhibitsByCategory(categoty);
    }

    public void changeState(Integer idExhibit, State newState, Integer idSection, Integer idOwner, Integer idPlace) {
        Exhibit exhibit = exhibitDao.getExhibitById(idExhibit);
        State oldState = exhibit.getState();
        deleteOldExhibitChilds(idExhibit, oldState);
        exhibit.setState(newState);
        exhibitDao.update(exhibit);
        switch (newState) {
            case exhibition: {
                exhibitDao.save(new MuseumExhibit(idExhibit, idPlace));
                break;
            }
            case onStock: {
                exhibitDao.save(new StoredExhibit(idExhibit, idSection));
                break;
            }
            case onExport: {
                exhibitDao.save(new ExportExhibit(idExhibit, idOwner));
                break;
            }
        }
    }

    private void deleteOldExhibitChilds(Integer idExhibit, State oldState) {
        switch (oldState) {
            case onStock: {
                exhibitDao.delete(exhibitDao.getStoredExhibitById(idExhibit));
                break;
            }
            case exhibition: {
                exhibitDao.delete(exhibitDao.getMuseumExhibitById(idExhibit));
                break;
            }
            case onExport: {
                exhibitDao.delete(exhibitDao.getExportExhibitById(idExhibit));
                break;
            }
        }
    }

    public void changePlace(Integer idExhibit, Integer idPlace) {
        MuseumExhibit museumExhibit = exhibitDao.getMuseumExhibitById(idExhibit);
        museumExhibit.setId_place(idPlace);
        exhibitDao.update(museumExhibit);
    }

    public ExhibitFullInformation getFullInformationExhibit(Integer idExhibit){
        Exhibit exhibit = exhibitDao.getExhibitById(idExhibit);
        Integer locationValue;
        switch (exhibit.getState()) {
            case onStock: {
                locationValue = exhibitDao.getStoredExhibitById(idExhibit).getLocationValue();
                return new ExhibitFullInformation(exhibit,locationValue, exhibitDao.getStoreById(locationValue).getSection());
            }
            case exhibition: {
                locationValue = exhibitDao.getMuseumExhibitById(idExhibit).getLocationValue();
                return new ExhibitFullInformation(exhibit,locationValue, exhibitDao.getPlaceById(locationValue).getPlace_name());
            }
            default: {
                locationValue = exhibitDao.getExportExhibitById(idExhibit).getLocationValue();
                return new ExhibitFullInformation(exhibit,locationValue,exhibitDao.getOwnerById(locationValue).getFio());
            }
        }
    }
}
