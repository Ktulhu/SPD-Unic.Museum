package com.museum.service;

import com.museum.dao.ExhibitDao;
import com.museum.model.CrunchifyFileUpload;
import com.museum.model.Exhibit;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
public class ImageService {

    @Autowired
    ExhibitDao exhibitDao;

    public String[] getImages(HttpServletRequest request) {
        ServletContext context = request.getSession().getServletContext();
        File[] files;
        java.awt.List imageList = new java.awt.List();

        File myFolder2 = new File(context.getRealPath("") + File.separator + "resources"+File.separator+"images"+File.separator+"");
        files = myFolder2.listFiles();
        if (files != null && files.length != 0) {
            for (File file : files) {
                String s = file.getPath();
                s = s.substring(s.indexOf("resources") - 1, s.length());
                imageList.add(s);
            }
        }
        return imageList.getItems();
    }

    public void saveImages(HttpServletRequest request, CrunchifyFileUpload image) throws IllegalStateException, IOException {
        ServletContext context = request.getSession().getServletContext();
        String saveDirectory = context.getRealPath("") + File.separator.substring(0, File.separator.length() - 1) + "resources"+File.separator+"images"+File.separator+"";
        List<MultipartFile> crunchifyFiles = image.getFiles();

        List<String> fileNames = new ArrayList<String>();

        if (null != crunchifyFiles && crunchifyFiles.size() > 0) {
            for (MultipartFile multipartFile : crunchifyFiles) {

                String fileName = multipartFile.getOriginalFilename();
                if (!"".equalsIgnoreCase(fileName)) {
                    multipartFile
                            .transferTo(new File(saveDirectory + fileName));
                    fileNames.add(fileName);
                }
            }
        }
    }

    public void deleteUnresolvedImages(HttpServletRequest request) {
        ServletContext context = request.getServletContext();
        List<Path> directoryFilePaths = null;
        List<String> directoryFileNames = new ArrayList<>();
        try {
            directoryFilePaths = Files.walk(Paths.get(context.getRealPath("") + "resources" + File.separator + "images" + File.separator + ""))
                    .filter(Files::isRegularFile)
                    .collect(Collectors.toList());
        } catch (IOException e) {
            e.printStackTrace();
        }
        directoryFilePaths.forEach(fileName -> directoryFileNames.add(fileName.toString()));

        List<String> fullPathsOfActiveImages = exhibitDao.getActiveImages();
        fullPathsOfActiveImages.forEach(s -> fullPathsOfActiveImages.set(fullPathsOfActiveImages.indexOf(s),//todo substring right got sequence
                context.getRealPath("")
                        .concat(s
                                .replace("/", File.separator)
                                .replace("\\", File.separator)
                                .substring(1, s.length()))));
        directoryFileNames.removeAll(fullPathsOfActiveImages);
        directoryFileNames.forEach(path -> {
            File deletingImage = new File(path);
//            deletingImage.delete();
        });
    }
}
