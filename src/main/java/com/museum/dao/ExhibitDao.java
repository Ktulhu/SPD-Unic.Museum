package com.museum.dao;

import com.museum.model.*;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Property;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class ExhibitDao {
    private final
    SessionFactory sessionFactory;

    @Autowired
    public ExhibitDao(@Qualifier(value = "sessionFactory") SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    public<T extends Exhibitble> void save(T exhibit) {
        sessionFactory.getCurrentSession().save(exhibit);
    }

    public<T extends Exhibitble> void delete(T exhibit) {
        sessionFactory.getCurrentSession().delete(exhibit);
    }

    public<T extends Exhibitble> void update(T exhibit) {
        sessionFactory.getCurrentSession().saveOrUpdate(exhibit);
    }

    public List<Exhibit> getListExhibitsByOwn(String fio) {
            List<Exhibit> exhibits= new ArrayList<>();
            int ownId = getIdOwnerByFio(fio);
            Criteria criteria = sessionFactory.getCurrentSession().createCriteria(ExportExhibit.class);
            criteria.add(Restrictions.eq("id_owner", ownId));
            criteria.list().forEach(o -> exhibits.add(getExhibitById(((ExportExhibit) o).getIdExhibit())));
        return exhibits;
    }

    private int getIdOwnerByFio(String fio) {
        Query query = sessionFactory.getCurrentSession()
                .createQuery(" select id_owner from Owners WHERE fio=:owner");
        query.setParameter("owner", fio);
        return (int) query.uniqueResult();
    }

    public Exhibit getExhibitById(Integer id) {
//        Exhibit exhibit = getByKey(id); //variant with abstractDao working
        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(Exhibit.class);
        criteria.add(Restrictions.eq("id_exhibit", id));
        return (Exhibit) criteria.uniqueResult();
    }

    public Store getStoreById(Integer id) {
        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(Store.class);
        criteria.add(Restrictions.eq("id_section", id));
        return (Store) criteria.uniqueResult();
    }
    public Place getPlaceById(Integer id) {
        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(Place.class);
        criteria.add(Restrictions.eq("id_place", id));
        return (Place) criteria.uniqueResult();
    }
    public Owners getOwnerById(Integer id) {
        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(Owners.class);
        criteria.add(Restrictions.eq("id_owner", id));
        return (Owners) criteria.uniqueResult();
    }

    public StoredExhibit getStoredExhibitById(Integer id) {
        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(StoredExhibit.class);
        criteria.add(Restrictions.eq("id_exhibit", id));
        return (StoredExhibit) criteria.uniqueResult();
    }

    public MuseumExhibit getMuseumExhibitById(Integer id) {
        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(MuseumExhibit.class);
        criteria.add(Restrictions.eq("id_exhibit", id));
        return (MuseumExhibit) criteria.uniqueResult();
    }

    public ExportExhibit getExportExhibitById(Integer id) {
        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(ExportExhibit.class);
        criteria.add(Restrictions.eq("id_exhibit", id));
        return (ExportExhibit) criteria.uniqueResult();
    }


    public List getFixedAmountExhibit(int count) {
        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(Exhibit.class);
        criteria.add(Restrictions.in("id_exhibit", getIdMuseumExhibits()));
        criteria.addOrder(Property.forName("id_exhibit").desc());
        criteria.setMaxResults(count);
        return criteria.list();
    }

    @SuppressWarnings("uncheked")
    public List<Exhibit> getAllExhibits() {
        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(Exhibit.class);
        return criteria.addOrder(Property.forName("id_exhibit").desc()).list();
    }

    public List getAllOwners() {
        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(Owners.class);
        return criteria.addOrder(Property.forName("id_owner").desc()).list();
    }

    public List<Place> getAllPlaces() {
        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(Place.class);
        return criteria.list();
    }

    public List<Store> getAllStores() {
        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(Store.class);
        return criteria.list();
    }

    public List<Exhibit> getListExhibitsByCategoryExceptStored(String category) {
        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(Exhibit.class);
        criteria.add(Restrictions.eq("category", category));
        List<Exhibit> exhibits = criteria.list();
        exhibits.removeAll(getStoredExhibits());
        return exhibits;
    }

    public List<Exhibit> getListExhibitsByCategory(String category) {
        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(Exhibit.class);
        criteria.add(Restrictions.eq("category", category));
        return criteria.list();
    }

    public List<Categories> getAllCategories() {
        return sessionFactory.getCurrentSession()
                .createQuery(" select distinct category from Categories ")
                .list();
    }

    public List<Exhibit> getMuseumExhibits() {
        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(Exhibit.class);
        criteria.add(Restrictions.in("id_exhibit", getIdMuseumExhibits()));
        return criteria.list();
    }

    private List<Integer> getIdMuseumExhibits() {
        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(MuseumExhibit.class);
        List<Integer> id_exhibits = new ArrayList<>();
        for (Object museumExhibit : criteria.list()) {
            MuseumExhibit m = (MuseumExhibit) museumExhibit;
            id_exhibits.add(m.getIdExhibit());
        }
        return id_exhibits;
    }

    public List<Exhibit> getExportExhibits() {
        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(Exhibit.class);
        criteria.add(Restrictions.in("id_exhibit", getIdExportExhibits()));
        return criteria.list();
    }

    private List<Integer> getIdExportExhibits() {
        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(ExportExhibit.class);
        List<Integer> id_exhibits = new ArrayList<>();
        for (Object exportExhibit : criteria.list()) {
            ExportExhibit m = (ExportExhibit) exportExhibit;
            id_exhibits.add(m.getIdExhibit());
        }
        return id_exhibits;
    }

    public List<Exhibit> getStoredExhibits() {
        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(Exhibit.class);
        criteria.add(Restrictions.in("id_exhibit", getIdStoredExhibits()));
        return criteria.list();
    }

    private List<Integer> getIdStoredExhibits() {
        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(StoredExhibit.class);
        List<Integer> id_exhibits = new ArrayList<>();
        for (Object storedExhibit : criteria.list()) {
            StoredExhibit m = (StoredExhibit) storedExhibit;
            id_exhibits.add(m.getIdExhibit());
        }
        return id_exhibits;
    }

    public List<String> getActiveImages() {
        List<String> stringSet = new ArrayList<>();
        for (Exhibit exhibit :
                 getAllExhibits()) {
             stringSet.add(exhibit.getImage());
        }
        return stringSet;
    }
}
