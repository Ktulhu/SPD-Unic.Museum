package com.museum.dao;

import com.museum.model.User;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

@Repository("userDao")
public class UserDao extends AbstractDao<Integer, User> {

    public User findUserById(int id) {
        return getByKey(id);
    }

    public User findUserBySso(String sso_id) {
        Criteria criteria = createEntityCriteria();
        criteria.add(Restrictions.eq("sso_id", sso_id));
        return (User) criteria.uniqueResult();
    }
}
