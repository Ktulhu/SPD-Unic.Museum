/*
Navicat MySQL Data Transfer

Source Server         : test
Source Server Version : 50711
Source Host           : localhost:777
Source Database       : museum

Target Server Type    : MYSQL
Target Server Version : 50711
File Encoding         : 65001

Date: 2017-08-31 14:41:57
*/

SET FOREIGN_KEY_CHECKS=0;
-- ----------------------------
-- Table structure for `categories`
-- ----------------------------
DROP TABLE IF EXISTS `categories`;
CREATE TABLE `categories` (
  `category` varchar(255) NOT NULL,
  `id_category` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id_category`),
  KEY `category` (`category`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of categories
-- ----------------------------
INSERT INTO `categories` VALUES ('Pistures', '1');
INSERT INTO `categories` VALUES ('skulptures', '2');

-- ----------------------------
-- Table structure for `ecscurtion`
-- ----------------------------
DROP TABLE IF EXISTS `ecscurtion`;
CREATE TABLE `ecscurtion` (
  `language` varchar(30) NOT NULL,
  `id_staff` int(11) DEFAULT NULL,
  `id_ecscurtion` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id_ecscurtion`),
  KEY `staff_ecscurFK` (`id_staff`),
  CONSTRAINT `staff_ecscurFK` FOREIGN KEY (`id_staff`) REFERENCES `staff` (`Id_staff`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of ecscurtion
-- ----------------------------

-- ----------------------------
-- Table structure for `ecscurtion_place`
-- ----------------------------
DROP TABLE IF EXISTS `ecscurtion_place`;
CREATE TABLE `ecscurtion_place` (
  `id_place` int(11) NOT NULL,
  `id_ecscurtion` int(11) NOT NULL,
  PRIMARY KEY (`id_place`,`id_ecscurtion`),
  KEY `ecscurtion_placeFK` (`id_ecscurtion`),
  CONSTRAINT `ecscurtion_placeFK` FOREIGN KEY (`id_ecscurtion`) REFERENCES `ecscurtion` (`id_ecscurtion`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `plcae_ecscurtionFK` FOREIGN KEY (`id_place`) REFERENCES `place` (`id_place`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of ecscurtion_place
-- ----------------------------

-- ----------------------------
-- Table structure for `exhibit`
-- ----------------------------
DROP TABLE IF EXISTS `exhibit`;
CREATE TABLE `exhibit` (
  `id_exhibit` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL DEFAULT '',
  `about` varchar(255) DEFAULT '',
  `state` varchar(255) NOT NULL DEFAULT 'exhibition',
  `category` varchar(50) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `id_owner` int(11) NOT NULL,
  `id_place` int(11) NOT NULL,
  `id_section` int(11) NOT NULL,
  PRIMARY KEY (`id_exhibit`),
  KEY `id_category` (`category`),
  CONSTRAINT `categoryFK` FOREIGN KEY (`category`) REFERENCES `categories` (`category`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of exhibit
-- ----------------------------
INSERT INTO `exhibit` VALUES ('7', 'Mona Liza', 'famos pizture', 'exhibition', 'Pistures', '\\resources\\images\\Mona_Lisa.jpg ', '0', '0', '0');
INSERT INTO `exhibit` VALUES ('8', 'Мальчик, вытаскивающий занозу', 'about boy', 'exhibition', 'skulptures', '\\resources\\images\\zanoza.JPG ', '0', '0', '0');
INSERT INTO `exhibit` VALUES ('10', 'Антиной Капитолийский', 'about this guy', 'exhibition', 'skulptures', '\\resources\\images\\kapitonskiy.jpg ', '0', '0', '0');
INSERT INTO `exhibit` VALUES ('12', ' cb vvb c', 'vbc bc v', 'exhibition', 'skulptures', '\\resources\\images\\Mona_Lisa.jpg ', '1', '1', '1');

-- ----------------------------
-- Table structure for `exhibit_move`
-- ----------------------------
DROP TABLE IF EXISTS `exhibit_move`;
CREATE TABLE `exhibit_move` (
  `id_exhibit` int(11) NOT NULL,
  `date` varchar(30) NOT NULL,
  `additional info` varchar(100) DEFAULT NULL,
  `id_move_exhibit` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id_move_exhibit`),
  KEY `exhibitFK` (`id_exhibit`),
  CONSTRAINT `exhibitFK` FOREIGN KEY (`id_exhibit`) REFERENCES `exhibit` (`id_exhibit`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of exhibit_move
-- ----------------------------

-- ----------------------------
-- Table structure for `export_exhibit`
-- ----------------------------
DROP TABLE IF EXISTS `export_exhibit`;
CREATE TABLE `export_exhibit` (
  `Id_exhibit` int(11) NOT NULL DEFAULT '0',
  `entry_date` varchar(30) NOT NULL DEFAULT '',
  `return_date` varchar(30) NOT NULL DEFAULT '',
  `id_owner` int(11) NOT NULL,
  PRIMARY KEY (`Id_exhibit`),
  KEY `exhibit_ownerFK` (`id_owner`),
  CONSTRAINT `exhibit_ownerFK` FOREIGN KEY (`id_owner`) REFERENCES `owners` (`id_owner`) ON UPDATE CASCADE,
  CONSTRAINT `export_exhibit_ibfk_1` FOREIGN KEY (`Id_exhibit`) REFERENCES `exhibit` (`id_exhibit`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of export_exhibit
-- ----------------------------

-- ----------------------------
-- Table structure for `museum_exhibit`
-- ----------------------------
DROP TABLE IF EXISTS `museum_exhibit`;
CREATE TABLE `museum_exhibit` (
  `id_place` int(11) NOT NULL,
  `id_exhibit` int(11) NOT NULL,
  PRIMARY KEY (`id_exhibit`),
  KEY `place_exhibitFK` (`id_place`),
  CONSTRAINT `museum_ExhibitFK` FOREIGN KEY (`id_exhibit`) REFERENCES `exhibit` (`id_exhibit`) ON UPDATE CASCADE,
  CONSTRAINT `place_exhibitFK` FOREIGN KEY (`id_place`) REFERENCES `place` (`id_place`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of museum_exhibit
-- ----------------------------

-- ----------------------------
-- Table structure for `owners`
-- ----------------------------
DROP TABLE IF EXISTS `owners`;
CREATE TABLE `owners` (
  `id_owner` int(11) NOT NULL AUTO_INCREMENT,
  `FIO` varchar(255) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `phone` varchar(30) DEFAULT NULL,
  `contract_number` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_owner`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of owners
-- ----------------------------
INSERT INTO `owners` VALUES ('1', 'museum', 'cherkassy', '23123', '23');
INSERT INTO `owners` VALUES ('2', 'someOwner', 'Paris', '324', '234');

-- ----------------------------
-- Table structure for `place`
-- ----------------------------
DROP TABLE IF EXISTS `place`;
CREATE TABLE `place` (
  `id_place` int(11) NOT NULL AUTO_INCREMENT,
  `place_name` varchar(100) NOT NULL,
  `about` varchar(255) DEFAULT NULL,
  `id_staff` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_place`),
  KEY `staffFK` (`id_staff`),
  CONSTRAINT `staffFK` FOREIGN KEY (`id_staff`) REFERENCES `staff` (`Id_staff`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of place
-- ----------------------------
INSERT INTO `place` VALUES ('1', 'Main hall', 'it\'s main place, where exhibits are placed', null);
INSERT INTO `place` VALUES ('2', 'Second hall', 'Located near main hall', null);

-- ----------------------------
-- Table structure for `staff`
-- ----------------------------
DROP TABLE IF EXISTS `staff`;
CREATE TABLE `staff` (
  `Id_staff` int(11) NOT NULL AUTO_INCREMENT,
  `position` varchar(255) DEFAULT '',
  `FIO` varchar(255) DEFAULT NULL,
  `wage` double(4,2) NOT NULL DEFAULT '0.00',
  `hire_date` varchar(30) DEFAULT NULL,
  `languages` varchar(200) DEFAULT NULL,
  `stage` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`Id_staff`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of staff
-- ----------------------------

-- ----------------------------
-- Table structure for `store`
-- ----------------------------
DROP TABLE IF EXISTS `store`;
CREATE TABLE `store` (
  `id_staff` int(11) DEFAULT NULL,
  `id_section` int(11) NOT NULL AUTO_INCREMENT,
  `section` varchar(100) NOT NULL,
  PRIMARY KEY (`id_section`),
  KEY `store_staffFK` (`id_staff`),
  CONSTRAINT `store_staffFK` FOREIGN KEY (`id_staff`) REFERENCES `staff` (`Id_staff`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of store
-- ----------------------------
INSERT INTO `store` VALUES (null, '1', 'not on store');
INSERT INTO `store` VALUES (null, '2', 'Broken');
INSERT INTO `store` VALUES (null, '3', 'Damaged');
INSERT INTO `store` VALUES (null, '4', 'Restavration');

-- ----------------------------
-- Table structure for `stored_exhibit`
-- ----------------------------
DROP TABLE IF EXISTS `stored_exhibit`;
CREATE TABLE `stored_exhibit` (
  `id_section` int(11) NOT NULL,
  `id_exhibit` int(11) NOT NULL,
  `store_date` varchar(30) NOT NULL,
  PRIMARY KEY (`id_exhibit`),
  KEY `this_storeFK` (`id_section`),
  CONSTRAINT `this_exhibitFK` FOREIGN KEY (`id_exhibit`) REFERENCES `exhibit` (`id_exhibit`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `this_storeFK` FOREIGN KEY (`id_section`) REFERENCES `store` (`id_section`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of stored_exhibit
-- ----------------------------
