<%@ page language="java" contentType="text/html; charset=utf-8"
         pageEncoding="ISO-8859-1" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <link href="<c:url value='../../resources/css/style.css' />" rel="stylesheet">
    <link href="<c:url value='../../resources/css/bootstrap.css' />" rel="stylesheet">
    <link href="<c:url value='../../resources/css/jquery.fancybox-1.3.0.css' />" rel="stylesheet">
    <title>Museum</title>

    <script language="javascript" type="text/javascript"
            src="http://ajax.googleapis.com/ajax/libs/jquery/1.2.6/jquery.min.js"></script>
    <script type="text/javascript" src="../../resources/js/jquery.fancybox-1.3.0.pack.js"></script>
    <script language="javascript" type="text/javascript" src="../../resources/js/Main.js"></script>
</head>

<body>
<div id="header">

    <div id="social">
        <a href="#"><img src="../../resources/styleimages/facebook.png" width="26" height="26" alt="facebook"/></a>
        <a href="#"><img src="../../resources/styleimages/twitter.png" width="26" height="26" alt="twitter"/></a>
        <a href="#"><img src="../../resources/styleimages/linkedin.png" width="26" height="26" alt="linkedin"/></a>
        <a href="#"><img src="../../resources/styleimages/email.png" width="26" height="26" alt="email"/></a>
    </div>

</div>


<ul class="dropdown"><!-- menu -->

    <li><a href="${pageContext.request.contextPath}/">Home</a></li>

    <li><a href="${pageContext.request.contextPath}/gallery">Gallery</a>
        <ul class="sub_menu">
            <li><a>Category</a>
                <ul>
                    <c:forEach items="${categoriesList}" var="category">
                        <li>
                            <a href="${pageContext.request.contextPath}/gallery?sort=category&value=${category}">${category}</a>
                        </li>
                    </c:forEach>
                </ul>
            </li>
            <li><a href="${pageContext.request.contextPath}/gallery?sort=state&value=Exhibition">Exhibition</a></li>
            <li><a href="${pageContext.request.contextPath}/gallery?sort=state&value=On%20Export">On export</a></li>
        </ul>
    </li>
    <!-- end submenu -->
    <li><a href="${pageContext.request.contextPath}/saveUpdate?id=0">NewExhibit</a></li>
</ul><!-- close menu -->


<div id="content"><!-- content -->

    <div class="ex_coll1_item">
        <div class="ex_title">
            <p>${exhibit.title}</p>
        </div>
        <div class="ex_image">
            <img style="" width="200px" height="265px" src="<c:url value='${exhibit.image}'/> " alt="loading.."/>
        </div>
    </div>
    <div class="ex_coll2_item">
        <div class="ex_top_banner">
        <div class="ex_category">
            <p>Category:${exhibit.category}</p>
        </div>

        </div>
        <div class="ex_about">
            <p class="dline">${exhibit.about}</p>
        </div>
    </div>



</div><!-- end content -->


<div id="sidebar"><!-- sidebar -->

    <h3>Recent</h3>

    <div class="navcontainer">
        <ul>
            <li><a href="#">Lorem ipsum dolor</a></li>
            <li><a href="#">Maecenas leo risus</a></li>
            <li><a href="#">Donec ac felis</a></li>
            <li><a href="#">Proin interdum</a></li>
            <li><a href="#">Morbi dapibus erat</a></li>
        </ul>
    </div>

    <h3>Archive</h3>

    <div class="navcontainer">
        <ul>
            <li><a href="#">February</a></li>
            <li><a href="#">January</a></li>
            <li><a href="#">December</a></li>
            <li><a href="#">November</a></li>
            <li><a href="#">October</a></li>
        </ul>
    </div>

    <h3></h3>

    <h3></h3>

</div><!-- end sidebar -->


<div id="footer">
    Exclusively on <a href="http://www.sixrevisions.com">Six Revisions</a> | Created by <a
        href="http://www.csstemplateheaven.com">CssTemplateHeaven</a>
</div>

</body>
</html>
